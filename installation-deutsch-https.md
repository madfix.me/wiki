<!-- TITLE: Installation -->
<!-- SUBTITLE: Installation des Material Design WebInterfacemit Https -->
# Installation
Zuvor müssen wir alle Schritte wie bei der Normalen Installation machen. 
Siehe: [Installation](installation-deutsch)

Bei einer Installation mit Https nur ein Paar Sachen ändern.

Wir gehen in unserem WebSerevr(Nginx oder Apache) und erstellen uns einem Reverse Proxy Eintrag.
Nginx
```text
location /some/path/to/cloudnet/master {
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_pass http://CLOUDNET_MASTER_IP/DOMAIN:1420;
}
```

Apache2

```text
ProxyPass "/CLOUD"  "http://IP VOM MASTER:1420"
ProxyPassReverse "/CLOUD"  "http://IP VOM MASTER:1420"
```
Bei Apche müssen wor noch den `mod_proxy` aktivieren mit `a2em mod_proxy`

Sobald wir den Pfad angepasst haben, können wir in die Config datei. 
Dort ändern wir vom CloudServer die URL ab auf die, wie wir beim Reverse Proxy eingestellt haben. 
Speichern, Seite neuladen und Sofort können wir die Seite Via HTTPs Benutzen.

