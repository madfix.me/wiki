<!-- TITLE: Installation -->
<!-- SUBTITLE: Installation des Material Design WebInterface -->
# Installation
#### Normale Installation
Das Material Design WebInterface, ist eine erweiterung für CloudNet(v2) um via WebInterface zu Steueren und zu Überwachen.
In folgenden Schritten erklären Wir dir wie du Unser WebInterface aufsetzt.

###### Installation des Modules(RestfulAPI Erweiterung)
Um alle Funktionen der WebInterfaces nutzen zu können, benötigst du unser Module was die RestApi von CloudNet erweitert.

Unter der Adresse [download.madfix.me](https://download.madfix.me) kannst du das Module downloaden.
Wähle beim Selector `CLOUDNET` aus(Siehe bild).
Drücke danach auf Next(Weiter).
Den folgenden Schritt können wir übersprigen(Siehe bild). Also drücken wir wieder auf next
Im nächsten Schritt wählen wir `RELEASE`, dies ist der Update Channel den wir später noch brauchen.
Er gibt drei weiter Update Channel:
* RELEASE - Immer eine Stabile Version
* Snapshot - Ist immer eine Version die in der Entwicklung ist, in der kann es zu instabilität führen. Diese version ist zum Testen und Fehler melden bevor sie als Release veröffentlicht wird.
* Beta - Ist eine Version die zur sehr Instabile Version mit neuen Funktionen
* Alpha - Ist ein Version die zu abstürtzen, Problemen, Sicherheits Fehlern verursachen kann. Diese version ist eine Version in sehr fürhen entwicklungs stadium mit neuen Funktionen, die Hinzugefügt wurden aber bei Release oder Beta entfernt werden
Im letzen schritt wählen wir noch unsere Version aus die wir haben wollen, die Reihnfolge ist von Neu-nach-alt sortiert.
Nun klicken wir auf Download und ziehen diese im Modul Ordner vom MASTER.

Im MASTER in der Console geben wir nun `reload all` ein.
Nun werden wir nach dem Update Channel gefragt(Frage: `Which UpdateChannel you want to use?(RELEASE,BETA,ALPHA,SNAPSHOT) | string` ) dort geben wir in  Capslock `RELEASE` ein(Dies ist der geliche Channel wie beim Download).

Nun müssen eine Config erstellen für das WebInterface das machen wir mit `setupI`.
Dort werden wir nun nach verschieden Informationen gefragt.

* `Please insert the network name of the cloud | string` - Bitte gebe den Netwerk name ein
Damit ist gefragt wie ihr Minecraft Netzwerk ist. Dies wird Später in der Auswahl liste stehen.
Ich gebe aus Installation beispiel `Testing` ein.

* `If you will enabled Google Recaptcha ? | yes : no` - Möchtest du Google Recaptcha benutzen?
Mit `yes` oder `no` können wir sagen ob wir es benutzt wollen.
Wir sagen in unserem beispiel `no`

* `Please insert the key for Google Recaptcha | string` - Bitte gebe den Google Recaptcha Key ein
Dies brauchen man falls wir Google Recapcha verwenden wollen
Wir geben einfach ein `X` ein

* `Please insert the default theme for WebInterface(dark-theme|light-theme|mad-theme) | string` - Welches Standard Theme soll das WebInterface sein?
Dies ist das default Theme für das Interface, wenn man sich zum erstenmal anmeldet.
Wir nehem das `mad-theme`(Schwarz-Balu und Gelb).

* `Please insert the session timeout for WebInterface Session(In Minutes) | number` - Wie lange soll die Login session sein bevor der Nutzer abgemeldet werden soll?
Dies ist in Minuten angegeben wie lange die Login Session ist
Wir geben dort `15` ein

* `Please insert the Branding for WebInterface | string` - Wie soll das Web Seiten Branding sein?
Mit diesem String könnt ihr eure Web interface Personalisieren

* `Please enter the update interval in milliseconds for the console live update. | number` - Wie soll der Intervall sein zum Updaten der Live Konsole?
Mit diesem Wert können sie die Geschwinidkeit der Konsole bestimmen.

* `Please enter the update interval in milliseconds for the dashboard live update. | number` - Wie soll der Intervall sein zum Updaten des Dashboard sin?
Mit diesem Wert können sie die Geschwinidkeit des Dashboard bestimmen.


Nun ist der Setup fertig für das Modul, wir haben die Config generiert. Diese liegt unter `MASTER_FOLDER/local/mdwi/config.json`.
###### Installation des Interface

Nun Laden wir nach dem Gleichen Schema wie oben das WebInterface herunter. Wir tauschen nur beim ersten Selektor von `CloudNET` zu `Web`.

Sobald wir die Zip datei heruntergeladen haben, entpacken wir sie auf einem WebSpace oder WebServer.
Nun haben wir dort ein `assets/config` ordner. Dort hinein kopieren wir die Config vom Master. Die wir zuvor Genriert haben.
Nun können wir mit der Ip oder Domain das WebInterface unter dem Pfad beutzen wo es auf dem WebServer liegt.
