<!-- TITLE: Permissions -->
<!-- SUBTITLE: A quick summary of Permissions -->

# Permissions
| Permission | Description | Placeholder | Placeholder Description |
| ------------ | -------------| ------------- | -------------------------- |
| cloudnet.web.cperms.info.group.*  | Get all groups infos(Permission,Prefix etc) of CPerms Groups   | - | - | 
| cloudnet.web.cperms.info.group.GROUP_NAME | Get specified info about the group | GROUP_NAME |  placeholder replace with specified group | 
| cloudnet.web.cperms.groups | Get all groups / Give CPerms access control | - | - |
