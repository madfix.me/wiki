<!-- TITLE: Installation English -->
<!-- SUBTITLE: English Installation -->

Normal installation
The material Design WebInterface, is an extension for CloudNet (v2) to control and monitor via WebInterface.
In the following steps we explain how you set up our web interface.

 Installation of the module (RestfulAPI extension)
In order to use all functions of the WebInterfaces, you need our module which extends the RestApi of CloudNet.

You can download the module at the address download.madfix.me.
Select CLOUDNET from the selector (see picture).
Then press Next.
We can skip the following step (see picture). So we press again on next
In the next step we choose RELEASE, this is the update channel we will need later.
He gives three more update channel:

RELEASE - Always a stable version
Snapshot - Is always a version that is under development in which it can lead to instability. This version is for testing and reporting errors before it is released as a release.
Beta - Is a version the most unstable version with new features
Alpha - Is a version that crashes to problems that can cause security errors. This version is a very developmental version with new features that have been added but will be removed on release or beta
In the last step we choose our version that we want, the order is sorted by new-to-old.
Now we click on Download and drag it in the module folder of the MASTER.
In the MASTER in the console, we now reload all.
Now we are asked for the Update Channel (Question: Which UpdateChannel you want to use? (RELEASE, BETA, ALPHA, SNAPSHOT) | string) where we enter in Capslock RELEASE (This is the same channel as in the download).

Now you have to create a config for the web interface that we do with setupI.
There we will be asked for different information.

Please insert the network name of the cloud | string - Please enter the network name
This is asking how your Minecraft network is. This will appear later in the selection list.
I enter testing for example from installation.

If you want enabled Google Recaptcha? | yes: no - Do you want to use Google Recaptcha?
With yes or no we can say if we want to use it.
We say no in our example

Please insert the key for Google Recaptcha | string - Please enter the Google Recaptcha Key
This is necessary if we want to use Google Recapcha
We just enter an X.

Please insert the default theme for WebInterface (dark-theme | light-theme | mad-theme) | string - Which standard theme should the web interface be?
This is the default theme for the interface when logging in for the first time.
We take the mad-theme (black bala and yellow).

Please insert the session timeout for WebInterface Session (In Minutes) | number - How long should the login session be before the user should be logged out?
This indicates in minutes how long the login session is
We enter 15 there

Please insert the Branding for WebInterface | string - How should the web pages be branding?
With this string you can personalize your web interface

Now the setup is ready for the module, we have generated the config. This is under MASTER_FOLDER / local / mdwi / config.json.

 Installation of the interface