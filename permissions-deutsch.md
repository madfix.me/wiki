<!-- TITLE: Permissions Deutsch -->
<!-- SUBTITLE: Eine Tabelle an Rechten -->

# Rechte
| Permission 	| Beschreibung 	| Version |
|----------: |:-------------|--------------- |
| **Allgemeine Rechte**|||
|`*`| Gibt dem benutzer alle Rechte	|Alle|
| **CloudNet Permission System**||0.11.X|
|`cloudnet.web.cperms.info.group.*`	| Gibt dem User die Berechtigung alle Gruppen vom Permission System die Informationen abzurufen|0.11.X|
|`cloudnet.web.cperms.info.group.(Gruppe)`|Gibt dem User die Berechtigung nur von dieser Gruppen die Informationen abzurufen|011.X|
|`cloudnet.web.cperms.groups`| Gibt dem User die Berechtigung alle Gruppen in der Liste zu sehen|0.11.X|
|`cloudnet.web.cperms.info.groups.*`| Gibt dem User die Berechtigung alle Gruppen im Vorschlag Menü zu sehen|0.11.X|
|`cloudnet.web.cperms.info.user.*`| Gibt dem User die Berechtigung alle Spieler zu bearbeiten zu dürfen|0.11.X|
|`cloudnet.web.cperms.info.user.(User/UUID)`|Gibt dem User die Berechtigung nur von dem einen sie zu Bearbeiten zu dürfen|0.11.X|
|`cloudnet.web.cperms.check`|Gibt dem User die Berechtigung das Permission System zu benutze|0.11.X|
|`cloudnet.web.cperms.group.save.*`|Gibt dem User die Berechtigung die geänderte Gruppe zu speichern|0.11.X|
|`cloudnet.web.cperms.group.save.(Gruppe)`|Gibt dem User die Berechtigung nur die Gruppe speichern zu dürfen|0.11.X|
|`cloudnet.web.cperms.group.delete.*`|Gibt dem User die Berechtigung alle Gruppen löschen zu können|0.11.X|
|`cloudnet.web.cperms.group.delete.(Gruppe)`|Gibt dem User die Berechtigung nur die Gruppe löschen zu drüfen|0.11.X|
|`cloudnet.web.cperms.user.save.*`|Gibt dem User die Berechtigung alle Spieler nach dem Bearbeiten, speichern zu drüfen|0.11.X|
|`cloudnet.web.cperms.user.save.(Spielername/UUID)`|Gibt dem User die Berechtigung nur den Spieler nach dem Beareniten, speichern zu dürfen|0.11.X|
|**MobSelektor**||1.1.0|
|`cloudnet.web.module.mob.load`|Gibt dem User die Berechtigung die Config datei auszulesen und das Mob Modul zu benutzen|1.1.0|
|`cloudnet.web.module.mob.save`|Gibt dem User die Berechtigung die Config zu Speichern|1.1.0|
|`cloudnet.web.module.mob.delete`|Gibt dem User die Berechtigungen ein Mob zu löschen|1.1.0|
|`cloudnet.web.module.mob.add`|Gibt dem User die Berechtigung ein Mob zu erstellen|1.1.0|
|**Spieler Senden**||0.12.X|
|`cloudnet.web.player.send`|Gibt dem User die Berechtigung Spieler senden zu dürfen|0.12.X|
|`cloudnet.web.player.send.*`|Gibt dem User die Berechtigung alle Spieler senden zu dürfen|0.12.X|
|`cloudnet.web.player.send.(UUID/Spieler name)`|Gibt dem User die Berechtigung nur den Spieler sende zu dürfen|0.12.X|
|**Proxy Gruppen**||Alle|
|`cloudnet.web.group.proxy.item.*`|Gibt dem User die Berechtigungen alle Proxy Gruppen aufzulisten|Alle|
|`cloudnet.web.group.proxy.item.(Gruppe)`|Gibt dem User die Berechtigungen die Proxy Gruppen aufzulisten|Alle|
|`cloudnet.web.group.proxys`|Gibt dem User die Berechtigung Proxy Vorschläge zu benutzen|Alle|
|`cloudnet.web.group.proxy.info.*`| Gibt dem User die Berechtigung von allen Proxy Gruppen Informationen abzufrufen|Alle|
|`cloudnet.web.group.proxy.info.(Gruppe)`| Gibt dem User die Berechtigung von der Proxy Gruppe Informationen abzufrufen|Alle|
|`cloudnet.web.screen.proxys.info.**` |Gibt dem User die Berechtigung von alle Proxys die Konsole zu öffnen|Alle|
|`cloudnet.web.screen.proxys.info.(Gruppe)`|Gibt dem User die Berechtigung von der Gruppe alle Proxy Konsolen zu öffnen|Alle|
|`cloudnet.web.group.proxys.info.*`|Gibt dem User die Berechigung alle Proxys aufzulisten|Alle|
|`cloudnet.web.group.proxys.info.(Gruppe)`|Gibt dem User die Berechtigung alle Proxys der Gruppe aufzulisten|Alle|
|`cloudnet.web.screen.proxy.command.*`|Gibt dem User die Berechtigung ein alle Commands in die Proxy Konsole zu senden|Alle|
|`cloudnet.web.screen.proxy.command.(Command)`|Gibt dem User die Berechtigung den Command in die Proxy Konsole zu senden|Alle|
|`cloudnet.web.group.proxy.stop.*`|Gibt dem User die Berechtigung alle Proxys von alle Proxy Gruppen Stoppen zu können|Alle|
|`cloudnet.web.group.proxy.stop.(Gruppe)`|Gibt dem User die Berechtigung alle Proxys der Gruppe zu Stoppen zu können|Alle|
|`cloudnet.web.group.proxy.delete.*`|Gibt dem User die Berechtigung alle Proxy Gruppen löschen zu können|Alle|
|`cloudnet.web.group.proxy.delete.(Gruppe)`|Gibt dem User die Berechtigung die Gruppe zu Löschen|Alle|
|`cloudnet.web.group.proxy.save.*`|Gibt dem User die Berechtigung alle Gruppe zu Speicher oder eine zu erstellen|Alle|
|`cloudnet.web.group.proxy.save.(Gruppe)`|Gibt dem User die Berechtigung nur die Gruppe zu speicher oder zu erstellen|Alle|
|`cloudnet.web.group.proxy.start.*`|Gibt dem User die Berechtigung von allen Proxy Gruppen, Proxys starten zu können|Alle|
|`cloudnet.web.group.proxy.start.(Gruppe)`|Gibt dem User die Berechtigung von der Gruppe, Proxys starten zu können|Alle|
|**Server Gruppen**||Alle|
|`cloudnet.web.group.servers`|Gibt dem User die Berechtigung alle Server Gruppen zu sehen|Alle|
|`cloudnet.web.screen.servers.info.*`|Gibt dem User die Berechtigung alle Server Kosnolen zu öffnen|Alle|
|`cloudnet.web.screen.servers.info.(Gruppe)`|Gibt dem User die Berechtigung von der Gruppe Konsolen zu öffnen|Alle|
|`cloudnet.web.group.servers.info.*`|Gibt dem User die Berechtigung Informationen von allen Server Gruppen abzurufen|Alle|
|`cloudnet.web.group.servers.info.(Gruppe)`|Gibt dem User die Berechtigung Informationen von der Server Gruppen abzurufen|Alle|
|`cloudnet.web.group.allservers.info.*`|Gibt dem User die Berechtigung alle Server als Liste abzurufen.|Alle|
|`cloudnet.web.group.server.item.*`|Gibt dem User die Berechtigung alle Server Gruppe zu sehen|Alle|
|`cloudnet.web.group.server.item.(Gruppe)`|Gibt dem User die Berechtigung nur die Server Gruppe zu sehen|Alle|
|`cloudnet.web.group.server.stop.*`|Gibt dem User die Berechtigung alle Server Stoppen zu können|Alle|
|`cloudnet.web.group.server.stop.(Gruppe)`|Gibt dem User die Berechtigung alle Server der Gruppe stoppen zu können|Alle|
|`cloudnet.web.screen.server.command.*`|Gibt dem User die Berechtigung alle Commands in der Server konsole auszuführen|Alle|
|`cloudnet.web.screen.server.command.(Command)`|Gibt dem User die Berechtigung den Command in der Server Konsole auszuführen|Alle|
|`cloudnet.web.group.server.delete.*`|Gibt dem User die Berechtigung alle Server Gruppen löschen zu können|Alle|
|`cloudnet.web.group.server.delete.(Gruppe)`|Gibt dem User die Berechtigung die Server Gruppe zu löschen|Alle|
|`cloudnet.web.group.server.save.*`|Gibt dem User die Berechtigung alle Gruppen Speichern zu können oder eine zu erstellen|Alle|
|`cloudnet.web.group.server.save.(Gruppe)`|Gibt dem User die Berechtigung die Gruppe zu Speichern oder zu erstellen|Alle|
|`cloudnet.web.group.server.start.*`|Gibt dem User die Berechtigung Server von allen Gruppen zu Starten|Alle|
|`cloudnet.web.group.server.start.(Gruppe)`|Gibt dem User die Brechtigung von der Gruppe Server zu starten|Alle|
|**Schilder Verwaltung**||1.0.0|
|`cloudnet.web.module.sign.load`|Gibt dem User die Berechtigung das Schilder System zu nutzer und die Config zu laden|1.0.0.|
|`cloudnet.web.module.sign.save`|Gibt dem User die Berechtigung die Config zu speichern|1.0.0|
|`cloudnet.web.module.sign.delete.*`|Gibt dem User die Berechtigung alle Schilder Löschen zu können|1.0.0|
|`cloudnet.web.module.sign.add`|Gibt dem User die Berechtigung ein Schild hinzuzufügen|1.0.0|
|**Benutzer Verwaltung**||Alle|
|`cloudnet.web.user.item.*`|Gibt dem User die Berechtigung alle Benutzer aufzulisten|Alle|
|`cloudnet.web.user.save.*`|Gibt dem User die Berechtigung alle Benutzer speicher zu können|Alle|
|`cloudnet.web.user.save.(User)`|Gibt dem User die Berechtigung den Benutzer zu speichern|Alle|
|`cloudnet.web.user.restepassword.*`|Gibt dem User die Berechtigung von alle Benutzern das Passwort zurücksetzen zu können|Alle|
|`cloudnet.web.user.restepassword.(User)`|Gibt dem User die Berechtigung das nur von dem Benutzer das Passwort zurückzusetzen|Alle|
|`cloudnet.web.user.add.*`|Gibt dem User die Berechtigung einen Benutzer zu erstellen|Alle|
|`cloudnet.web.user.delete.*`|Gibt dem User die Berechtigung alle Benutzer löschen zu können|Alle|
|`cloudnet.web.user.delete.(User)`|Gibt dem User die Brechtigung nur diesen Benutzer zu löschen|Alle|
